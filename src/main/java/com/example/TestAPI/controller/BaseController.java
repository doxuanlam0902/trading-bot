package com.example.TestAPI.controller;

import com.example.TestAPI.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseController {
    protected final Logger log = LoggerFactory.getLogger(BaseService.class);
}
