package com.example.TestAPI.controller;

import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.common.ParseHeaderUtils;
import com.edso.trading.lib.entities.HeaderInfo;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.service.TradeService;
import com.google.common.base.Strings;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/trade")
public class TradeController extends BaseController {

    private final TradeService tradeService;

    public TradeController(TradeService tradeService) {
        this.tradeService = tradeService;
    }

    @GetMapping("")
    public ResponseEntity<BaseResponse> TradeBookFromBinance(@RequestHeader Map<String, String> headers,
                                                             @RequestParam(name = "symbol") String symbol) {

        HeaderInfo info = ParseHeaderUtils.parse(headers);
        log.info("=>getTradeBook h: {}", info);

        BaseResponse response = new BaseResponse();
        response = tradeService.getMarketTradeFromBinance(symbol);

        log.info("<= getTradeBook head: {}, resp: {}", info, response);
        return ResponseEntity.ok().body(response);
    }
}
