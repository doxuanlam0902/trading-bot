package com.example.TestAPI.controller;

import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.common.ParseHeaderUtils;
import com.edso.trading.lib.entities.HeaderInfo;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.service.OrderService;
import com.google.common.base.Strings;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController extends BaseController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("")
    public ResponseEntity<BaseResponse> orderBookFromBinance(@RequestHeader Map<String, String> headers,
                                                            @RequestParam(value = "pair") String pair,
                                                            @RequestParam(value = "limit") Integer limit) {
        HeaderInfo info = ParseHeaderUtils.parse(headers);
        log.info("=>getOrderBook h: {}, pair: {}, limit: {}", info, pair, limit);

        BaseResponse response = new BaseResponse();

        if (Strings.isNullOrEmpty(pair) || limit == null || limit <= 0) {
            response.setFailed(info.getLanguage(), ErrorCodeDefs.ERROR_CODE_PARAMS_INVALID, "Params invalid !");
        } else {
            response = orderService.orderBookFromBinance(pair, limit);
        }

        log.info("<= getOrderBook head: {}, pair:{}, limit: {}, resp: {}", info, pair, limit, response);
        return ResponseEntity.ok().body(response);
    }
}
