package com.example.TestAPI.controller;


import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.common.ParseHeaderUtils;
import com.edso.trading.lib.entities.HeaderInfo;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.service.ChartService;
import com.google.common.base.Strings;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/Chart")
public class ChartController extends BaseController {

    private final ChartService chartService;

    public ChartController(ChartService chartService) {
        this.chartService = chartService;
    }

    @GetMapping("")
    public ResponseEntity<BaseResponse> ChartBookFromBinance(@RequestHeader Map<String, String> headers,
                                                             @RequestParam(value = "pair") String pair,
                                                             @RequestParam(value = "interval") String interval,
                                                             @RequestParam(value = "startTime", defaultValue = "-1") Long startTime,
                                                             @RequestParam(value = "endTime",defaultValue = "-1") Long endTime,
                                                             @RequestParam(value = "limit",defaultValue = "-1") Integer limit) {
        HeaderInfo info = ParseHeaderUtils.parse(headers);
        log.info("=>getChartBook h: {}, pair: {}, limit: {}", info, pair, interval);

        BaseResponse response = new BaseResponse();

        if (Strings.isNullOrEmpty(pair) || Strings.isNullOrEmpty(interval)) {
            response.setFailed(info.getLanguage(), ErrorCodeDefs.ERROR_CODE_PARAMS_INVALID, "Params invalid !");
        } else {
            response = chartService.getDataChartFromBinance(pair, interval,startTime,endTime,limit);
        }

        log.info("<= getChartBook head: {}, pair:{}, limit: {}, resp: {}", info, pair, interval, response);
        return ResponseEntity.ok().body(response);
    }
}
