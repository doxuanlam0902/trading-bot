package com.example.TestAPI.service;

import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.http.HttpSender;
import com.edso.trading.lib.http.HttpSenderImpl;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.entity.ConversionType;
import com.example.TestAPI.entity.RateLimit;
import com.example.TestAPI.entity.TradeHisResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChartServiceImpl extends BaseService implements ChartService {
    @Override
    public BaseResponse getDataChartFromBinance(String symbol , String interval, Long startTime, Long endTime, Integer limit) {
        symbol = symbol.replace("/","") ;
        TradeHisResponse response = new TradeHisResponse() ;
        String uri = "https://www.binance.com/api/v3/klines?symbol="+ symbol +"&interval=" + interval;
        if(startTime != -1) {
            uri = uri + "&startTime=" + startTime ;
        }
        if (endTime != -1) {
            uri = uri + "&endTime=" + endTime;
        }
        if (limit != -1) {
            uri = uri + "&limit=" + limit;
        }

        try {
            HttpSender sender = new HttpSenderImpl();
            Map<String, String> params = new HashMap<>();
            Map<String, String> headers = new HashMap<>();
            int index = 0;
            JsonArray res = null;
            while (index < 10) {
                res = sender.getArray(uri, headers, params);
                if (res != null) {
                    break;
                }
                Thread.sleep(500);
                index++;
            }
            ObjectMapper mapper = new ObjectMapper();
            List<List<?>> dataList = new ArrayList<>();
            if (res != null) {
                for (int i = 0; i < res.size(); i++) {
                    List<?> data = mapper.readValue(res.get(i).toString(), List.class);
                    dataList.add(data);
                }
            }
            response.fromListData(dataList);
            response.setResponse("Success");
            response.setType(100);
            response.setAggregated(false);
            response.setTimeFrom(response.getData().get(0).getTime());
            response.setTimeTo(response.getData().get(response.getData().size() - 1).getTime());
            response.setFirstValueInArray(true);

            ConversionType conversionType = new ConversionType();
            conversionType.setType("");
            conversionType.setConversionSymbol("force_direct");
            response.setConversionType(conversionType);

            response.setRateLimit(new RateLimit());
            response.setHasWarning(false);

            response.setSuccess();


        } catch (Exception ex) {
            logger.error("Ex: ", ex);
            response.setFailed(ErrorCodeDefs.SYSTEM_IS_BUSY, "System busy");
            return response;
        }

        return response;
    }
}
