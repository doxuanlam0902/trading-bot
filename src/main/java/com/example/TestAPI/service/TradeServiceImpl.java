package com.example.TestAPI.service;

import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.http.HttpSender;
import com.edso.trading.lib.http.HttpSenderImpl;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.entity.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TradeServiceImpl extends BaseService implements TradeService{
    @Override
    public BaseResponse getMarketTradeFromBinance(String symbol) {
        symbol = symbol.replace("/","") ;
        ListTradeResponse response = new ListTradeResponse() ;
        String uri = "https://api.binance.com/api/v3/trades?symbol=" + symbol.replace("/", "");

        try {
            HttpSender sender = new HttpSenderImpl();
            Map<String, String> params = new HashMap<>();
            Map<String, String> headers = new HashMap<>();
            int index = 0;
            JsonArray res = null;
            while (index < 10) {
                res = sender.getArray(uri, headers, params);
                if (res != null) {
                    break;
                }
                Thread.sleep(500);
                index++;
            }
            ObjectMapper mapper = new ObjectMapper() ;
            List<TradeReceiver> data = new ArrayList<>();
            if(res != null) {
                for (int i = 0; i < res.size(); i++) {
                    TradeReceiver tradeReceiver = mapper.readValue(res.get(i).toString(), TradeReceiver.class);
                    data.add(tradeReceiver);
                }
            }
            DataMarketTradeReceiver listData = new DataMarketTradeReceiver() ;
            listData.setTradeReceivers(data);
            response.fromListData(listData);

            response.setSuccess();


        }  catch (Exception ex) {
            logger.error("Ex: ", ex);
            response.setFailed(ErrorCodeDefs.SYSTEM_IS_BUSY, "System busy");
            return response;
        }

        return  response ;
    }
}
