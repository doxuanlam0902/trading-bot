package com.example.TestAPI.service;

import com.edso.trading.lib.response.BaseResponse;

public interface ChartService {

    BaseResponse getDataChartFromBinance(String symbol , String interval,Long startTime, Long endTime, Integer limit ) ;
}
