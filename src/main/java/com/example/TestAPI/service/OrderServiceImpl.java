package com.example.TestAPI.service;

import com.edso.trading.lib.common.ErrorCodeDefs;
import com.edso.trading.lib.http.HttpSender;
import com.edso.trading.lib.http.HttpSenderImpl;
import com.edso.trading.lib.response.BaseResponse;
import com.example.TestAPI.entity.ListDataOrderBookReceiver;
import com.example.TestAPI.entity.ListOrderBook;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class OrderServiceImpl extends BaseService implements OrderService {
    @Override
    public BaseResponse orderBookFromBinance(String pair, Integer limit) {
        pair = pair.replace("/","") ;
        ListOrderBook response = new  ListOrderBook() ;
        JsonObject result = null;
        String uri = "https://api.binance.com/api/v3/depth?limit="+ limit + "&symbol="+ pair;

        try {
            HttpSender sender = new HttpSenderImpl();
            Map<String, String> params = new HashMap<>();
            Map<String, String> headers = new HashMap<>();
            int index = 0;
            while (index < 10) {
                result = sender.get(uri, headers, params);
                if (result != null) {
                    break;
                }
                Thread.sleep(500);
                index++;
            }

            System.out.println(result);
            ObjectMapper mapper = new ObjectMapper();
            if (result != null) {
                ListDataOrderBookReceiver listData = mapper.readValue(result.toString(), ListDataOrderBookReceiver.class);
                response.formListData(listData);
                response.setSuccess();
            }

        } catch (Exception ex) {
            logger.error("Ex: ", ex);
            response.setFailed(ErrorCodeDefs.SYSTEM_IS_BUSY, "System busy");
            return response;
        }

        return response;
    }
}
