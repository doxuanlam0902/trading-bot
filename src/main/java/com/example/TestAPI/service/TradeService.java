package com.example.TestAPI.service;

import com.edso.trading.lib.response.BaseResponse;

public interface TradeService {

    public BaseResponse getMarketTradeFromBinance(String symbol);
}
