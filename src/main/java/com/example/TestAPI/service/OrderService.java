package com.example.TestAPI.service;

import com.edso.trading.lib.response.BaseResponse;

public interface OrderService  {
    BaseResponse orderBookFromBinance(String pair, Integer limit) ;
}
