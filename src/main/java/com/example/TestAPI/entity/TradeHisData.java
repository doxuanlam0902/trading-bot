package com.example.TestAPI.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeHisData {
    private Long time;
    private Double close;
    private Double high;
    private Double low;
    private Double open;
    private Double volumeFrom;
    private Double volumeTo;
    private String conversionType;
    private String conversionSymbol;

//    public TradeHis fromDataToTradeHis() {
//        TradeHis tradeHis = new TradeHis();
//
//        tradeHis.setTime(new Date(time));
//        tradeHis.setClose(close);
//        tradeHis.setHigh(high);
//        tradeHis.setLow(low);
//        tradeHis.setOpen(open);
//        tradeHis.setVolumeFrom(volumeFrom);
//        tradeHis.setVolumeTo(volumeTo);
//        tradeHis.setConversionType(conversionType);
//        tradeHis.setConversionSymbol(conversionSymbol);
//
//        return tradeHis;
//    }
}
