package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ListDataOrderBookReceiver {
    private Long lastUpdateId;
    private List<List<String>> bids = new ArrayList<>();
    private List<List<String>> asks = new ArrayList<>();
}
