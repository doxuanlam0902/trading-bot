package com.example.TestAPI.entity;

public interface ISession {

    String getId();

    boolean send(Object data);

    void setLastTime(long lastTime);

    long getLastTime();

    void disconnect();

    void close();

    boolean isOpen();

}
