package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ask {
    private String price ;
    private String quantity ;

    public Ask(String price, String quantity) {
        this.price = price ;
        this.quantity = quantity ;
    }
}
