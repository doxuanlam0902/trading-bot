package com.example.TestAPI.entity;

import com.edso.trading.lib.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TradeHisResponse extends BaseResponse {
    @JsonProperty("Response")
    private String response;
    @JsonProperty("Type")
    private Integer type;
    @JsonProperty("Aggregated")
    private Boolean aggregated;
    @JsonProperty("TimeTo")
    private Long timeTo;
    @JsonProperty("TimeFrom")
    private Long timeFrom;
    @JsonProperty("FirstValueInArray")
    private Boolean firstValueInArray;
    @JsonProperty("ConversionType")
    private ConversionType conversionType;
    @JsonProperty("Data")
    private List<TradeHisData> data = new ArrayList<>();
    @JsonProperty("RateLimit")
    private RateLimit rateLimit;
    @JsonProperty("HasWarning")
    private Boolean hasWarning;


    public void fromListData(List<List<?>> data){
        for (int i=0 ; i< data.size() ; i++) {
//            1499040000000,      // Open time
//            "0.01634790",       // Open
//            "0.80000000",       // High
//            "0.01575800",       // Low
//            "0.01577100",       // Close
//            "148976.11427815",  // Volume
//            1499644799999,      // Close time
//            "2434.19055334",    // Quote asset volume
//             308,                // Number of trades
//            "1756.87402397",    // Taker buy base asset volume
//            "28.46694368",      // Taker buy quote asset volume
//            "17928899.62484339" // Ignore.
            TradeHisData tradeHisData = new TradeHisData() ;

            tradeHisData.setTime(Long.valueOf(data.get(i).get(0).toString()) / 1000);
            tradeHisData.setClose(Double.valueOf(data.get(i).get(4).toString()));
            tradeHisData.setHigh(Double.valueOf(data.get(i).get(2).toString()));
            tradeHisData.setLow(Double.valueOf(data.get(i).get(3).toString()));
            tradeHisData.setOpen(Double.valueOf(data.get(i).get(1).toString()));
            tradeHisData.setVolumeFrom(Double.valueOf(data.get(i).get(7).toString()));
            tradeHisData.setVolumeTo(Double.valueOf(data.get(i).get(5).toString()));
            tradeHisData.setConversionType("force_direct");
            tradeHisData.setConversionSymbol("");

            this.data.add(tradeHisData) ;
        }

    }
}
