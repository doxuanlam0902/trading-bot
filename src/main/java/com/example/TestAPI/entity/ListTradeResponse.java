package com.example.TestAPI.entity;

import com.edso.trading.lib.response.BaseResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ListTradeResponse extends BaseResponse {

    private int size ;
    private List<TradeResponse> list = new ArrayList<>();

    public void fromListData(DataMarketTradeReceiver data) {
        for(int i=0 ; i< data.getTradeReceivers().size() ; i++) {
            TradeResponse tradeResponse = new TradeResponse() ;
            tradeResponse.setAmount(data.getTradeReceivers().get(i).getQty());
            tradeResponse.setIsPriceUp(null);
            tradeResponse.setMarker_id("");
            tradeResponse.setPrice(data.getTradeReceivers().get(i).getPrice());
            tradeResponse.setTaker_id("");
            tradeResponse.setTotal_value(data.getTradeReceivers().get(i).getQuoteQty());
            tradeResponse.setTrade_id(data.getTradeReceivers().get(i).getId().toString());
            tradeResponse.setTs(data.getTradeReceivers().get(i).getTime());
            tradeResponse.setType("");
            tradeResponse.setUser_maker_id("");
            tradeResponse.setUser_taker_id("");

            this.list.add(tradeResponse) ;
        }
        this.size = list.size() ;
    }
}
