package com.example.TestAPI.entity;

import lombok.Data;

@Data
public class TradeResponse {

    private Double amount;
    private Boolean isPriceUp;
    private String marker_id;
    private Double price;
    private String taker_id;
    private Double total_value;
    private String trade_id;
    private Long ts;
    private String type;
    private String user_maker_id ;
    private String user_taker_id;

}
