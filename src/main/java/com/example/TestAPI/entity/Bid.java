package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bid {
    private String price ;
    private String quantity ;

    public Bid(String price, String quantity) {
        this.price = price ;
        this.quantity = quantity ;
    }
}
