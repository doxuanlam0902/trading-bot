package com.example.TestAPI.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeBinance {
    public String e;
    @JsonProperty("E")
    public Long e1;
    public String s;
    public Long t;
    public String p;
    public String q;
    public Long b;
    public Long a;
    @JsonProperty("T")
    public Long t1;
    public Boolean m;
    @JsonProperty("M")
    public Boolean m1;
}
