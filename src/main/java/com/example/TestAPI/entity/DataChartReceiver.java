package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DataChartReceiver {

    private List<List<String>> data ;
}
