package com.example.TestAPI.entity;

import com.edso.trading.lib.response.BaseResponse;
import lombok.Data;

@Data
public class ListOrderBook extends BaseResponse {
    private OrderBookData data = new OrderBookData() ;

    public void formListData(ListDataOrderBookReceiver listData) {
        for(int i =0 ; i< listData.getAsks().size() ; i++){
            Ask ask = new Ask(listData.getAsks().get(i).get(0),listData.getAsks().get(i).get(1)) ;
            this.data.getAsks().add(ask) ;
        }

        for(int i =0 ; i< listData.getBids().size() ; i++){
            Bid bid = new Bid(listData.getBids().get(i).get(0),listData.getBids().get(i).get(1)) ;
            this.data.getBids().add(bid) ;
        }
    }
}
