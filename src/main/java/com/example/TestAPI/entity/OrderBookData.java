package com.example.TestAPI.entity;

import com.example.TestAPI.entity.Ask;
import com.example.TestAPI.entity.Bid;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OrderBookData {

    private List<Ask> asks = new ArrayList<>();
    private List<Bid> bids = new ArrayList<>();
}
