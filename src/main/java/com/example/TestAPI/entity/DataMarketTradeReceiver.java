package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DataMarketTradeReceiver {
    private List<TradeReceiver> tradeReceivers = new ArrayList<>();
}

