package com.example.TestAPI.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TradeReceiver {
    private Long id;
    private Double price;
    private Double qty;
    private Double quoteQty;
    private Long time;
    private Boolean isBuyerMaker;
    private Boolean isBestMatch;
}
