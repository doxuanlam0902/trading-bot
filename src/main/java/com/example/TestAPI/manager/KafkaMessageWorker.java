package com.example.TestAPI.manager;

import com.edso.trading.lib.common.ThreadBase;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

public class KafkaMessageWorker extends ThreadBase {

    private final BlockingQueue<String> queue;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Gson gson = new Gson();

    public KafkaMessageWorker(String name, BlockingQueue<String> queue) {
        super(name);
        this.queue = queue;
    }

    @Override
    protected void action() throws Exception {

        String msg = queue.poll();
        if (msg == null) {
            return;
        }

        logger.info("receive data: {}", msg);

        try {

            JsonObject jo = gson.fromJson(msg, JsonObject.class);
            if (jo == null) {
                return;
            }

            String walletId = "";
            String username = "";

            JsonObject data = null;
            if (jo.has("data") && jo.get("data") != null && jo.get("data").isJsonObject()) {
                data = jo.getAsJsonObject("data");
            }

            if (data == null) {
                return;
            }

            if (jo.has("walletId") && jo.get("walletId") != null) {
                walletId = jo.get("walletId").getAsString();
            }

            if (jo.has("username") && jo.get("username") != null) {
                username = jo.get("username").getAsString();
            }

            if (!Strings.isNullOrEmpty(username)) {
                int sent = SessionManager.sendToUser(username, data.toString());
                logger.info("sent msg to username: {}, result: {}", username, sent);
            }

            if (!Strings.isNullOrEmpty(walletId)) {
                int sent = SessionManager.sendToWalletAddress(walletId, data.toString());
                logger.info("sent msg to walletId: {}, result: {}", walletId, sent);
            }

        } catch (Throwable ex) {
            logger.error("Ex: ", ex);
        }

    }

    @Override
    protected void onExecuting() throws Exception {
        logger.info("{} onKilling", getName());
    }

    @Override
    protected void onKilling() {
        logger.info("{} onKilling", getName());
    }

    @Override
    protected void onException(Throwable t) {
        logger.info("{} onException: ", getName(), t);
    }

    @Override
    protected long sleepTime() throws Exception {
        return 150;
    }

}
