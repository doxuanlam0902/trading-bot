package com.example.TestAPI.manager;

import org.slf4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class KafkaMessageManager {

    private static final KafkaMessageManager ourInstance = new KafkaMessageManager();
    private final BlockingQueue<String> queue;
    private Logger logger;

    private KafkaMessageManager() {
        this.queue = new LinkedBlockingDeque<>();
    }

    public static KafkaMessageManager getInstance() {
        return ourInstance;
    }

    public void setup(Logger logger) {
        this.logger = logger;
        this.init();
    }

    private void init() {
        int numWorker = 20;
        for (int i = 0; i < numWorker; i++) {
            String name = String.format("KafkaMessageWorker %d", (i + 1));
            KafkaMessageWorker worker = new KafkaMessageWorker(name, queue);
            worker.execute();
        }
        if (this.logger != null) {
            this.logger.info("setup block chain event queue success");
        }
    }

    public boolean put(String event) {
        return this.queue.offer(event);
    }

}
