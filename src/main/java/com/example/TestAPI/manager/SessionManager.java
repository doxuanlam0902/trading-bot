package com.example.TestAPI.manager;

import com.example.TestAPI.entity.ISession;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class SessionManager {

    private static final Logger logger = LoggerFactory.getLogger(SessionManager.class);

    private static final ConcurrentHashMap<String, List<ISession>> userSessionMap = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, List<ISession>> walletSessionMap = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, List<ISession>> pairSessionMap = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, List<ISession>> pairChartSessionMap = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, List<ISession>> tradeNotiSessionMap = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, ISession> SESSIONS = new ConcurrentHashMap<>();

    private static final ReentrantLock LOCK = new ReentrantLock(true);
    private static final ReentrantLock LOCK_WALLET = new ReentrantLock(true);
    private static final ReentrantLock LOCK_PAIR = new ReentrantLock(true);
    private static final ReentrantLock LOCK_PAIR_CHART = new ReentrantLock(true);
    private static final ReentrantLock LOCK_P2P_TRADE = new ReentrantLock(true);

    private SessionManager() {

    }

    public static int sendToUser(String userId, String msg) {
        return _sendTo(userSessionMap, userId, msg);
    }

    public static int sendToWalletAddress(String walletAddress, String msg) {
        return _sendTo(walletSessionMap, walletAddress, msg);
    }

    public static int sendToPair(String pair, String msg) {
        return _sendTo(pairSessionMap, pair, msg);
    }

    public static int sendToPairChart(String pairChart, String msg) {
        return _sendTo(pairChartSessionMap, pairChart, msg);
    }

    public static int sendToP2PTradeNotification(String tradeId, String msg) {
        return _sendTo(tradeNotiSessionMap, tradeId, msg);
    }

    private static int _sendTo(Map<String, List<ISession>> mapConn, String to, String msg) {
        checkHeartbeat();
        if (mapConn.isEmpty()) {
            return -1;
        }
        List<ISession> list = mapConn.get(to);
        if (list == null || list.isEmpty()) {
            return -1;
        }
        int count = 0;
        for (ISession session : list) {
            boolean sent = false;
            int i = 0;
            while (!sent && i < 10) {
                sent = session.send(msg);
                i++;
            }

            if (sent) {
                count++;
            }
        }
        logger.debug("Total sent to: {}, is: {}", to, count);
        return count;
    }

    public static void sendAll(String msg) {
        checkHeartbeat();
        if (SESSIONS.isEmpty()) {
            return;
        }
        long count = 0;
        for (ISession session : SESSIONS.values()) {
            boolean sent = session.send(msg);
            if (sent) {
                count++;
            }
        }
        logger.debug("Total sent: {}", count);
    }

    public static void addSession(String userId, List<String> walletIds, String pair, String pairChart, ISession session) {
        SESSIONS.put(session.getId(), session);
        if (!Strings.isNullOrEmpty(userId)) {
            LOCK.lock();
            try {
                _addSessionToMapConn(userSessionMap, userId, session);
            } finally {
                LOCK.unlock();
            }
        }
        if (walletIds != null && !walletIds.isEmpty()) {
            LOCK_WALLET.lock();
            try {
                for (String walletId : walletIds) {
                    _addSessionToMapConn(walletSessionMap, walletId, session);
                }
            } finally {
                LOCK_WALLET.unlock();
            }
        }
        if (!Strings.isNullOrEmpty(pair)) {
            LOCK_PAIR.lock();
            try {
                _addSessionToMapConn(pairSessionMap, pair, session);
            } finally {
                LOCK_PAIR.unlock();
            }
        }
        if (!Strings.isNullOrEmpty(pairChart)) {
            LOCK_PAIR_CHART.lock();
            try {
                _addSessionToMapConn(pairChartSessionMap, pairChart, session);
                SESSIONS.remove(session.getId(), session);
            } finally {
                LOCK_PAIR_CHART.unlock();
            }
        }
    }

    public static void addSessionP2PTrade(String userId, String tradeId, ISession session) {
        SESSIONS.put(session.getId(), session);
        if (!Strings.isNullOrEmpty(userId)) {
            LOCK.lock();
            try {
                _addSessionToMapConn(userSessionMap, userId, session);
            } finally {
                LOCK.unlock();
            }
        }
        if (!Strings.isNullOrEmpty(tradeId)) {
            LOCK_P2P_TRADE.lock();
            try {
                _addSessionToMapConn(tradeNotiSessionMap, tradeId, session);
                SESSIONS.remove(session.getId(), session);
            } finally {
                LOCK_P2P_TRADE.unlock();
            }
        }

    }

    public static void removeSession(String userId, List<String> walletIds, String pair, String pairChart, WebSocketSession session) {
        if (!Strings.isNullOrEmpty(userId)) {
            LOCK.lock();
            try {
                _remoteSessionFromMapConn(userSessionMap, userId, session);
            } finally {
                LOCK.unlock();
            }
        }
        if (walletIds != null && !walletIds.isEmpty()) {
            LOCK_WALLET.lock();
            try {
                for (String walletId : walletIds) {
                    _remoteSessionFromMapConn(walletSessionMap, walletId, session);
                }
            } finally {
                LOCK_WALLET.unlock();
            }
        }
        if (!Strings.isNullOrEmpty(pair)) {
            LOCK_PAIR.lock();
            try {
                _remoteSessionFromMapConn(pairSessionMap, pair, session);
            } finally {
                LOCK_PAIR.unlock();
            }
        }

        if (!Strings.isNullOrEmpty(pairChart)) {
            LOCK_PAIR_CHART.lock();
            try {
                _remoteSessionFromMapConn(pairChartSessionMap, pairChart, session);
            } finally {
                LOCK_PAIR_CHART.unlock();
            }
        }
    }

    public static void removeSessionP2PTrade(String userId, String tradeId, WebSocketSession session) {

        if (!Strings.isNullOrEmpty(tradeId)) {
            LOCK_P2P_TRADE.lock();
            try {
                _remoteSessionFromMapConn(tradeNotiSessionMap, tradeId, session);
            } finally {
                LOCK_P2P_TRADE.unlock();
            }
        }
    }

    private static void _addSessionToMapConn(Map<String, List<ISession>> mapConn, String key, ISession session) {
        List<ISession> sessions = mapConn.get(key);
        if (sessions == null) {
            sessions = new ArrayList<>();
            sessions.add(session);
            mapConn.put(key, sessions);
            return;
        }
        sessions.add(session);
        logger.info("Add session: {} to mapKey: {}", session.getId(), key);
    }

    private static void _remoteSessionFromMapConn(Map<String, List<ISession>> mapConn, String key, WebSocketSession session) {
        List<ISession> sessions = mapConn.get(key);
        if (sessions != null) {
            sessions.removeIf(s -> s.getId().equals(session.getId()));
            logger.info("Remote session: {} from mapKey: {}", session.getId(), key);
        }

    }

    // check hbc
    public static void checkHeartbeat() {

        logger.info("check heartbeat: {} sessions", SESSIONS.size());
        if (SESSIONS.isEmpty()) {
            return;
        }

        long now = System.currentTimeMillis();
        Set<String> removeSessions = new HashSet<>();
        for (Map.Entry<String, ISession> entry : SESSIONS.entrySet()) {
            ISession session = entry.getValue();
            long lastTime = session.getLastTime();
            if (now - lastTime > 20000 || !session.isOpen()) {
                logger.info("session: {} expire hbc -> disconnect", session.getId());
                session.disconnect();
                removeSessions.add(session.getId());
            }
        }

        if (!removeSessions.isEmpty()) {

            for (String id : removeSessions) {
                SESSIONS.remove(id);
            }

            LOCK.lock();
            try {
                _removeClosedConnection(userSessionMap, removeSessions);
            } finally {
                LOCK.unlock();
            }

            LOCK_WALLET.lock();
            try {
                _removeClosedConnection(walletSessionMap, removeSessions);
            } finally {
                LOCK_WALLET.unlock();
            }

            LOCK_PAIR.lock();
            try {
                _removeClosedConnection(pairSessionMap, removeSessions);
            } finally {
                LOCK_PAIR.unlock();
            }

            LOCK_PAIR_CHART.lock();
            try {
                _removeClosedConnection(pairChartSessionMap, removeSessions);
            } finally {
                LOCK_PAIR_CHART.unlock();
            }

        }

    }

    private static void _removeClosedConnection(Map<String, List<ISession>> mapConn, final Set<String> removed) {
        for (Map.Entry<String, List<ISession>> entry : mapConn.entrySet()) {
            List<ISession> sessions = entry.getValue();
            if (sessions == null || sessions.isEmpty()) {
                continue;
            }
            sessions.removeIf(s -> removed.contains(s.getId()));
        }
    }

    // remove empty sessions
    public static void removeEmptySessionList() {
        logger.info("Start clean sessions");
        LOCK.lock();
        try {
            _removeEmptyConn(userSessionMap);
        } finally {
            LOCK.unlock();
        }
        LOCK_WALLET.lock();
        try {
            _removeEmptyConn(walletSessionMap);
        } finally {
            LOCK_WALLET.unlock();
        }
        LOCK_PAIR.lock();
        try {
            _removeEmptyConn(pairSessionMap);
        } finally {
            LOCK_PAIR.unlock();
        }
        LOCK_PAIR_CHART.lock();
        try {
            _removeEmptyConn(pairChartSessionMap);
        } finally {
            LOCK_PAIR_CHART.unlock();
        }
    }

    private static void _removeEmptyConn(Map<String, List<ISession>> mapConn) {
        if (mapConn.isEmpty()) {
            return;
        }
        Set<String> removeUserIDs = new HashSet<>();
        for (Map.Entry<String, List<ISession>> entry : mapConn.entrySet()) {
            List<ISession> sessions = entry.getValue();
            if (sessions != null && !sessions.isEmpty()) {
                removeUserIDs.add(entry.getKey());
            }
        }
        if (!removeUserIDs.isEmpty()) {
            for (String id : removeUserIDs) {
                mapConn.remove(id);
            }
        }
    }

    public static void setLastAction(String id) {
        if (Strings.isNullOrEmpty(id)) {
            return;
        }
        ISession session = SESSIONS.get(id);
        if (session != null) {
            session.setLastTime(System.currentTimeMillis());
        }
    }

    public static void closeSession(String id) {
        if (Strings.isNullOrEmpty(id)) {
            return;
        }
        ISession session = SESSIONS.get(id);
        if (session != null) {
            session.close();
        }
    }

    public static void monitorSession() {
        int totalSession = SESSIONS.size();
        int totalUser = userSessionMap.size();
        int totalUserEmpty = _countEmptyList(userSessionMap);
        int totalWallet = walletSessionMap.size();
        int totalWalletEmpty = _countEmptyList(walletSessionMap);
        logger.info("Monitor totalSession: {}, totalUser: {}, totalUserEmpty: {}, totalWallet: {}, totalWalletEmpty: {}",
                totalSession, totalUser, totalUserEmpty, totalWallet, totalWalletEmpty);
    }

    private static int _countEmptyList(Map<String, List<ISession>> mapConn) {
        int count = 0;
        for (Map.Entry<String, List<ISession>> e : mapConn.entrySet()) {
            if (e.getValue().isEmpty()) {
                count++;
            }
        }
        return count;
    }

}
