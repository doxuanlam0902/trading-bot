package com.example.TestAPI.websocket.spring;

import com.example.TestAPI.entity.ISession;
import com.example.TestAPI.entity.SpringWSSession;
import com.example.TestAPI.manager.SessionManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Arrays;

@Service
public class WebSocketHandler extends TextWebSocketHandler {

    @Autowired
    private Gson gson = new Gson();

    public static WebSocketSession webSocketSession = null;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public WebSocketHandler() {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        logger.info("connection closed with status: {}", status.getCode());
        SessionManager.closeSession(session.getId());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {

    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage msg) {
        SessionManager.setLastAction(session.getId());
        String message = msg.getPayload();

        ISession client = new SpringWSSession(session);

        Thread listener = new Thread(() -> {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = null;
            try {
                node = mapper.readTree(message);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String method = null;
            String pair;
            String pairChart;
            if (node.has("type")) {
                String type = node.get("type").asText();
                String tradeId = node.get("tradeId").asText();
                if (type.equalsIgnoreCase("SUBSCRIBE")) {
                    SessionManager.addSessionP2PTrade("userId" + Math.random(), tradeId, client);
                } else {
                    SessionManager.removeSessionP2PTrade("userId" + Math.random(), tradeId, session);
                }

            }

            if (node.has("method")) {
                method = node.get("method").asText();
                if (method.equalsIgnoreCase("SUBSCRIBE")) {

                    if (node.has("pair")) {
                        pair = node.get("pair").asText();
                    } else {
                        pair = "";
                        logger.error("Pair is null or empty");
                    }

                    if (pair.contains("CHART")) {
                        pairChart = pair;
                        pair = "";
                    } else {
                        pairChart = "";
                    }

                    SessionManager.addSession("userId" + Math.random(), Arrays.asList(""), pair, pairChart, client);

                } else if (method.equalsIgnoreCase("UNSUBSCRIBE")) {

                    if (node.has("pair")) {
                        pair = node.get("pair").asText();
                    } else {
                        pair = "";
                        logger.error("Pair is null or empty");
                    }

                    if (pair.contains("CHART")) {
                        pairChart = pair;
                        pair = "";
                    } else {
                        pairChart = "";
                    }

                    SessionManager.removeSession("userId" + Math.random(), Arrays.asList(""), pair, pairChart, session);
                }
            }
        });
        listener.start();

        logger.info("session: {}, message: {}", session.getId(), message);
    }
}
