package com.example.TestAPI.websocket.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

public class ClientSocketHandshakeInterceptor implements HandshakeInterceptor {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse serverHttpResponse, org.springframework.web.socket.WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
//        URI uri = request.getURI();
//        logger.info("connection websocket uri: {}", uri);
//        Map<String, String> params = URLQueryUtils.getParams(uri);
//        String token = params.get("token");
//        if (Strings.isNullOrEmpty(token)) {
//            token = "";
//        }
//        map.put("g_token", token);
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {

    }

}
