package com.example.TestAPI.websocket.netty;//package com.edso.nft.websocket.netty;
//
//import io.netty.bootstrap.ServerBootstrap;
//import io.netty.channel.ChannelFuture;
//import io.netty.channel.ChannelInitializer;
//import io.netty.channel.EventLoopGroup;
//import io.netty.channel.nio.NioEventLoopGroup;
//import io.netty.channel.socket.SocketChannel;
//import io.netty.channel.socket.nio.NioServerSocketChannel;
//import io.netty.handler.codec.http.HttpObjectAggregator;
//import io.netty.handler.codec.http.HttpServerCodec;
//import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
//import io.netty.handler.codec.serialization.ObjectEncoder;
//import io.netty.handler.stream.ChunkedWriteHandler;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import java.net.InetSocketAddress;
//
//@Component
//public class NettyServer {
//
//    private final Logger logger = LoggerFactory.getLogger(NettyServer.class);
//
//    private static final String WEBSOCKET_PROTOCOL = "WebSocket";
//
//    @Value("${webSocket.netty.port:10010}")
//    private int port;
//
//    @Value("${webSocket.netty.path:/websocket}")
//    private String webSocketPath;
//
//    private final WebSocketHandler webSocketHandler;
//
//    private EventLoopGroup bossGroup;
//    private EventLoopGroup workGroup;
//
//    public NettyServer(WebSocketHandler webSocketHandler) {
//        this.webSocketHandler = webSocketHandler;
//    }
//
//    private void start() throws InterruptedException {
//        bossGroup = new NioEventLoopGroup();
//        workGroup = new NioEventLoopGroup();
//        ServerBootstrap bootstrap = new ServerBootstrap();
//        // For the tcp connection request of the secondary client of bossGroup, workGroup is responsible for the read and write operations before the client
//        bootstrap.group(bossGroup, workGroup);
//        // Set channel of NIO type
//        bootstrap.channel(NioServerSocketChannel.class);
//        // Set listening port
//        bootstrap.localAddress(new InetSocketAddress(port));
//        // A channel is created when the connection arrives
//        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
//
//            @Override
//            protected void initChannel(SocketChannel ch) throws Exception {
//                // The Handler in the pipeline management channel is used to handle business
//                // The webSocket protocol itself is based on the http protocol, so we also need to use the http codec here
//                ch.pipeline().addLast(new HttpServerCodec());
//                ch.pipeline().addLast(new ObjectEncoder());
//                // A processor written in blocks
//                ch.pipeline().addLast(new ChunkedWriteHandler());
//                /*
//                Explain:
//                1,http Data is segmented during transmission, and HttpObjectAggregator can aggregate multiple segments
//                2,That's why when a browser sends a lot of data, it sends multiple http requests
//                 */
//                ch.pipeline().addLast(new HttpObjectAggregator(8192));
//                /*
//                Explain:
//                1,Corresponding to webSocket, its data is passed in the form of frame
//                2,When the browser requests, ws://localhost:58080/xxx indicates the requested uri
//                3,The core function is to upgrade http protocol to ws protocol and maintain long connection
//                */
//                ch.pipeline().addLast(new WebSocketServerProtocolHandler(webSocketPath, WEBSOCKET_PROTOCOL, true, 65536 * 10));
//                // Custom handler, handling business logic
//                ch.pipeline().addLast(webSocketHandler);
//
//            }
//        });
//        // After the configuration is completed, start binding the server, and block by calling the sync synchronization method until the binding is successful
//        ChannelFuture channelFuture = bootstrap.bind().sync();
//        logger.info("Server started and listen on:{}", channelFuture.channel().localAddress());
//        // Monitor the closed channel
//        channelFuture.channel().closeFuture().sync();
//    }
//
//    @PreDestroy
//    public void destroy() throws InterruptedException {
//        if (bossGroup != null) {
//            bossGroup.shutdownGracefully().sync();
//        }
//        if (workGroup != null) {
//            workGroup.shutdownGracefully().sync();
//        }
//    }
//
//    @PostConstruct()
//    public void init() {
//        new Thread(() -> {
//            try {
//                start();
//            } catch (Throwable e) {
//                logger.error("Ex: ", e);
//            }
//        }).start();
//    }
//
//}
