package com.example.TestAPI.websocket.netty;//package com.edso.nft.websocket.websocket.netty;
//
//import com.edso.nft.websocket.service.ValidateUserService;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.SimpleChannelInboundHandler;
//import io.netty.handler.codec.http.HttpHeaders;
//import io.netty.handler.codec.http.websocketx.WebSocketFrame;
//import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
//import io.netty.util.AttributeKey;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Map;
//
//public class WebSocketHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
//
//    private final Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);
//    private final ValidateUserService validateUserService;
//
//    public WebSocketHandler(ValidateUserService validateUserService) {
//        this.validateUserService = validateUserService;
//    }
//
//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        super.channelActive(ctx);
//        logger.info("channelActive");
//    }
//
//    @Override
//    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
//        super.userEventTriggered(ctx, evt);
//        logger.info("userEventTriggered");
//        if (evt instanceof WebSocketServerProtocolHandler.HandshakeComplete) {
//            WebSocketServerProtocolHandler.HandshakeComplete handshake = (WebSocketServerProtocolHandler.HandshakeComplete) evt;
//
//            HttpHeaders headers = handshake.requestHeaders();
//
//            //http request uri: /chat?accesskey=hello
//            String uri = handshake.requestUri();
//
//            logger.info("uri: {}", uri);
//
//            //TODO: parse uri parameters to map ...
//            Map<String, String> params;
//
//            //put to channel context
////            ctx.channel().attr(RequestParams).set(params);
//
//        } else {
//            ctx.fireUserEventTriggered(evt);
//        }
//    }
//
////    @Override
////    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
////        logger.info("handlerAdded Be called" + ctx.channel().id().asLongText());
////        NettyConfig.getChannelGroup().add(ctx.channel());
////    }
//
//    @Override
//    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame msg) throws Exception {
////        logger.info("Server received message:{}", msg.text());
//        logger.info("receiveMessage  here");
////        // Get user ID and associate with channel
////        JSONObject jsonObject = JSONUtil.parseObj(msg.text());
////        String uid = jsonObject.getStr("uid");
////        NettyConfig.getUserChannelMap().put(uid, ctx.channel());
////        // The user ID is added to the channel as a custom attribute, which is convenient to obtain the user ID in the channel at any time
////        AttributeKey<String> key = AttributeKey.valueOf("userId");
////        ctx.channel().attr(key).setIfAbsent(uid);
////        // Reply message
////        ctx.channel().writeAndFlush(new TextWebSocketFrame("Server connection succeeded!"));
//    }
//
////    @Override
////    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
////        logger.info("handlerRemoved Be called" + ctx.channel().id().asLongText());
////        NettyConfig.getChannelGroup().remove(ctx.channel());
////        removeUserId(ctx);
////    }
//
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        logger.info("Exception:{}", cause.getMessage());
//        SessionManager.getChannelGroup().remove(ctx.channel());
//        removeUserId(ctx);
//        ctx.close();
//    }
//
//    private void removeUserId(ChannelHandlerContext ctx) {
//        AttributeKey<String> key = AttributeKey.valueOf("userId");
//        String userId = ctx.channel().attr(key).get();
//        SessionManager.getUserChannelMap().remove(userId);
//    }
//
//}
